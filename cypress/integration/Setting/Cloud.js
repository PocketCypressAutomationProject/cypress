class Cloud {

    verifyNotication(message) {
        cy.get(".toast-message").invoke('text').should('eq', message)
        cy.get(".toast-message").click({ force: true })
    }

    selectEmployee(employeeId) {
        cy.get('#select2-approvalManager-container').click({ force: true })
        cy.get('input[type="search"]').click({ force: true }).type(employeeId)
        cy.get('.select2-results__option--highlighted').click({ force: true })
    }

    verifyPageTitle(title) {
        cy.get('.card-header').invoke('text').should('contain', title)
    }

    rollAllocation(module, role) {
        cy.visit(Cypress.env('url') + 'Settings/Employee/ESSIndex?module=Profile&submodule=RoleAllocation')
        cy.wait(2000)
        cy.get('#drpModule').select(module)
        cy.wait(1000)
        cy.get('#drpRole').select(role)
        cy.wait(1000)
        cy.xpath("//button[contains(text(),'Search')]").click()
        cy.wait(1000)
        cy.get('[class="chk menu"]').each(function (row, i) {
            cy.get('[class="chk menu"]').eq(i).click({ force: true })
        })
        //click on save button
        cy.get('#savedata').click()
        //verify success message
        cy.get(".toast-message").invoke('text').then((text) => {
            expect(text.trim()).equal('Setting Save successfully')
        })
        //click on message to close message box
        cy.get(".toast-message").click()
        cy.wait(1000)
    }

}

export default Cloud