

/// <reference types="@shelex/cypress-allure-plugin" />
const allureWriter = require('@shelex/cypress-allure-plugin/writer');

module.exports = (on, config) => {
  allureWriter(on, config);
  return config;
};



const path = require("path");
const fs = require('fs')
PDFParser = require("pdf2json");
const tagify = require('cypress-tags');
const readXlsx = require('./read-xlsx')

module.exports = (on, config) => {

  on('task', {  
  
    'readXlsx': readXlsx.read,

    parseXlsx({ filePath }) {
      return new Promise((resolve, reject) => {
        try {
          const jsonData = xlsx.parse(fs.readFileSync(filePath));
          resolve(jsonData);
        } catch (e) {
          reject(e);
        }
      });
    },


    convertExcelToJson_CurrentFile({ file, fileName }) {
      var content = "{" + JSON.stringify(getFileTextData(fileName)) + "},\n";
      //fs.appendFileSync('D:/CypressPocketHRMS/cypress/downloads/'+file+'.txt', content);
      fs.appendFileSync('cypress/downloads/' + file + '.txt', content);
      // fs.appendFileSync('D:/CypressPocketHRMS/cypress/fixtures/'+file+'.txt', content);
      console.log("File Parsed Successfully");
      console.log("content: " + content);
      return content;
    },

    convertExcelToJson_ExitingFile({ file, fileName }) {

      var content = "{" + JSON.stringify(getFileTextData(fileName)) + "},\n";
      fs.appendFileSync('D:\\CyPress Demo\\cypress\\ExcelFiles\\Exiting_Downloads\\' + file + '.txt', content);
      console.log("File Parsed Successfully");
      console.log("content: " + content);
      return content;
    },

    convertPDFToJson_CurrentFile({ file, fileName }) {
      let pdfParser = new PDFParser(this, 1);

      pdfParser.loadPDF(fileName);
      pdfParser.on("pdfParser_dataReady", pdfData => {
        let content = pdfParser.getRawTextContent()
        fs.writeFile('D:\\CyPress Demo\\cypress\\ExcelFiles\\Current_Downloads\\' + file + '.txt', content);
      });
      return null;
    },

    convertPDFToJson_ExitingFile({ file, fileName }) {
      let pdfParser = new PDFParser(this, 1);

      pdfParser.loadPDF(fileName);
      pdfParser.on("pdfParser_dataReady", pdfData => {
        let content = pdfParser.getRawTextContent()
        fs.writeFile('D:\\CyPress Demo\\cypress\\ExcelFiles\\Exiting_Downloads\\' + file + '.txt', content);


      });
      return null;

    },

    deleteFile({ fileName }) {
      fs.unlinkSync(fileName);
      return null;
    },


    downloadFile1() {
      with (imports) {
        var path = "/Users/dave/IdeaProjects/Sikuli/images/";
        var eight = path + "eight.png";
        var multiply = path + "multiply.png";
        var five = path + "five.png";
        var three = path + "three.png";
        var equals = path + "equals.png";
        var result = path + "result.png";

        var Screen = Java.type("org.sikuli.script.Screen");
        var App = Java.type("org.sikuli.script.App");
        var MacUtil = Java.type("org.sikuli.natives.MacUtil");
        var Assert = Java.type("org.junit.Assert");

        var s = new Screen();
        var a = new App();
        var mu = new MacUtil();

        a.setName("Calculator");

        mu.open(a);

        s.click(eight);
        s.click(multiply);
        s.click(three);
        s.click(equals);
        Assert.assertNotNull("The Image is not Correct", s.exists(result));
      }
      return null;
    },


  }),
  
  on('file:preprocessor', tagify(config));
}


